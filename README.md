# unet-earrape

## Commands:  
`uer [earrape]`
Switches are formatted as `--verbosekey value` or `-shortkey value`. If the verbosy key was `count` with a short key of `c` and the value was `100`, you would use `--count 100` or `-c 100`.  
  
Switches can be inserted at any point in the command, but it has to be after the command (`uer`) itself. Example:  
`uer lcz --id 5 -c 1000`

## Earrapes:
* `lcz` - LCZ CASSIE announcements.
  * `--id` - ID of the announcement to play, `0`-`5`. Default `4` (the 30s one).
  * `--global` - Whether or not to play on players out of LCZ, supports `true` or `false`, `1` or `0`, and `yes` or `no`. Default `true`.
* `blink` - Forces players to blink, must be near 173 (client-side check).
* `chaos` - Chaos Insurgency drums.
* `106` - Femur breaker.
* `intercom` - Intercom start/ends noises
  * `--end` - Whether or not the sound is the sound played when the intercom normally ends. Default `true`.
* `elevator` - Elevator music. Does not support count/offset because client :(
* `ambient` - Plays an ambient sound from the sound pool.
  * `--id` - ID of the ambient sound to play, `0`-`24`. Default `24` (the SCP-079 whirring one).

### Source Earrapes
These earrapes allow for an additional switch, `--source`, which is used the same as `--selector` (a.k.a `-s`), but determines which players the sound should play from.
  
* `foot` - Footstep noise
  * `--running` - Whether or not the footstep should play from the running source. Default `true`.
* `fall` - Fall damage crack.
* `micro` - Micro HID charging sound.
* `939` - 939 growl noise. Only plays from 939s cause client-side check.
* `shoot` - Forces players to shoot their guns
  * `--weapon` - Weapon to shoot, default all. Note that the players must be holding this weapon.

## Switches for all commands:
* `--count` or `-c` - Amount of calls to run. Default `1`.
* `--offset` or `-o` - Interval between calls. Default `0`.
* `--selector` or `-s` - Player selector to apply it to. Inclues `*`, player ID, player name. Default `*`.
* `--filter` or `-f` - Filter to apply to the selector. Includes `global`, `radius`. Default `global`.
  * `--radius` or `-r` - Filters to players within radius. No default, must be set when radius filter is used.