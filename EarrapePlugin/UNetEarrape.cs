﻿using Smod2;
using Smod2.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using EarrapePlugin.Earrapes;
using Smod2.API;
using Smod2.Config;
using Smod2.Piping;

namespace EarrapePlugin
{
	[PluginDetails(
	author = "Androx & Citrio",
	name = "UNet-Earrape",
	description = "A plugin that adds many commands and functionalities to earrape players by exploiting UNet",
	id = "com.citrox.unetearrape",
	configPrefix = "uer",
	langFile = "unetearrape",
	version = "1.0",
	SmodMajor = 3,
	SmodMinor = 4,
	SmodRevision = 0
	)]
	public class UNetEarrape : Plugin
	{
		private BaseEarrape[] earrapes;
		public IEnumerable<BaseEarrape> Earrapes
		{
			get
			{
				foreach (BaseEarrape earrape in earrapes)
				{
					yield return earrape;
				}
			}
		}

		[ConfigOption]
		public readonly string[] whitelist =
		{
			"owner",
			"admin"
		};

		public override void Register()
		{
			Type[] ctorParameterTypes = {typeof(UNetEarrape), typeof(EarrapeInfo)};
			object[] ctorParameterInstances = {this, null};

			earrapes =
				AppDomain.CurrentDomain.GetAssemblies()
			         .SelectMany(x => x.GetTypes())
			         .Select(x =>
			         {
				         try
				         {
					         if (!typeof(BaseEarrape).IsAssignableFrom(x)) return null;

					         EarrapeInfo info = x.GetCustomAttribute<EarrapeInfo>();
					         if (info == null)
					         {
						         Debug($"Skipping {x}, does not have an {nameof(EarrapeInfo)} attribute attached.");
						         return null;
					         }

					         ConstructorInfo ctor = x.GetConstructor(ctorParameterTypes);
					         if (ctor == null)
					         {
						         Debug($"Skipping {x}, does not have a valid constructor.");
						         return null;
					         }

					         ctorParameterInstances[1] = info;
					         BaseEarrape earrape;
					         try
					         {
						         earrape = (BaseEarrape) ctor.Invoke(ctorParameterInstances);
					         }
					         catch (Exception e)
					         {
						         Error($"Failed to create instance of {info.Name} earrape:\n{e}");

						         return null;
					         }

					         return earrape;
				         }
				         catch (Exception e)
				         {
					         Warn($"Exception thrown while checking type {x}:\n{e}");

					         return null;
				         }
			         })
			         .Where(x => x != null)
			         .ToArray();

			RootCommand.Create(this, new[] {"uer", "earrape"});
		}

		public override void OnEnable()
		{
			Info($"Enabling {Details.name} version {Details.version}");
		}

		public override void OnDisable()
		{
			Info($"Disabling {Details.name} version {Details.version}");
		}

		[PipeMethod]
		public string[] EarrapePlayers(string name, IEnumerable<Player> players, string[] args, int count, float offset, Dictionary<string, string> switches)
		{
			if (name == null) throw new ArgumentNullException(nameof(name));
			if (players == null) throw new ArgumentNullException(nameof(players));
			if (args == null) throw new ArgumentNullException(nameof(args));
			if (switches == null) throw new ArgumentNullException(nameof(switches));

			BaseEarrape earrape = Earrapes.FirstOrDefault(x => x.Info.Name == name);
			if (earrape == null) throw new InvalidOperationException("Earrape not found.");

			return earrape.EarrapePlayers(players, args, count, offset, switches);
		}
	}
}
