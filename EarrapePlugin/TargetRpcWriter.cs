using System;
using UnityEngine;
using UnityEngine.Networking;

namespace EarrapePlugin
{
	public class TargetRpcWriter : NetworkWriter, IDisposable
	{
		private readonly int channel;
		private readonly NetworkConnection connection;

		private void InitMessage(uint id)
		{
			Write((short) 0); // Starter?
			Write((short) 2); // Server -> client verification
			WritePackedUInt32(id); // RPC ID, found in object that has RPC method
		}

		public TargetRpcWriter(GameObject target, uint id, int channel = 0)
		{
			this.channel = channel;
			connection = target.GetComponent<CharacterClassManager>().connectionToClient;

			InitMessage(id);
			Write(target.GetComponent<NetworkIdentity>().netId); // ID of eminating GameObject
		}

		public TargetRpcWriter(GameObject target, uint id, NetworkInstanceId source, int channel = 0)
		{
			this.channel = channel;
			connection = target.GetComponent<CharacterClassManager>().connectionToClient;

			InitMessage(id);
			Write(source); // ID of the eminating GameObject
		}

		public TargetRpcWriter(GameObject target, uint id, GameObject source, int channel = 0) : this(target, id, source.GetComponent<NetworkIdentity>().netId, channel) { }

		public void Dispose()
		{
			FinishMessage();
			connection.SendWriter(this, channel);
		}
	}
}