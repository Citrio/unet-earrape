using System;
using System.Collections.Generic;
using System.Linq;
using Smod2.API;
using Smod2.Commands;

namespace EarrapePlugin.Earrapes
{
	public class RootCommand : ICommandHandler
	{
		private static readonly Dictionary<string, SearchFilter> _filters;

		static RootCommand()
		{
			_filters =
				((SearchFilter[]) Enum.GetValues(typeof(SearchFilter)))
				.ToDictionary(x => x.ToString().ToLower(), x => x);
		}

		private enum SearchFilter
		{
			Global,
			Radius
		}

		private readonly UNetEarrape owner;
		private readonly string usage;

		private readonly string[] aliases;
		public IEnumerable<string> Aliases
		{
			get
			{
				foreach (string alias in aliases)
				{
					yield return alias;
				}
			}
		}

		private RootCommand(UNetEarrape owner, string[] aliases)
		{
			if (owner == null) throw new ArgumentNullException(nameof(owner));
			if (aliases == null) throw new ArgumentNullException(nameof(aliases));
			if (aliases.Length == 0) throw new ArgumentException("There must be at least one alias.", nameof(aliases));

			this.owner = owner;

			this.aliases = aliases;
			usage = $"{string.Join("/", aliases)} [player selector] [earrape instance] [earrape-specific arguments]";
		}

		public static RootCommand Create(UNetEarrape owner, string[] aliases)
		{
			RootCommand command = new RootCommand(owner, aliases);
			owner.AddCommands(aliases, command);

			return command;
		}

		private string[] PlayersFromFilter(Player sender, SearchFilter filter, IReadOnlyDictionary<string, string> switches, out IEnumerable<Player> players)
		{
			Func<Player, bool> predicate;

			switch (filter)
			{
				case SearchFilter.Global:
					predicate = player => true;
					break;

				case SearchFilter.Radius:
					if (sender == null)
					{
						players = null;
						return new[]
						{
							"Radius filter is only valid when used by a player."
						};
					}

					if ((!switches.TryGetValue("radius", out string radiusData) && !switches.TryGetValue("r", out radiusData)) || !int.TryParse(radiusData, out int radius))
					{
						players = null;
						return new[]
						{
							"Radius value is not a valid number."
						};
					}

					predicate = player => Vector.Distance(sender.GetPosition(), player.GetPosition()) < radius;
					break;

				default:
					throw new ArgumentOutOfRangeException(nameof(filter));
			}

			IEnumerable<Player> GetPlayers()
			{
				foreach (Player player in owner.Server.GetPlayers())
				{
					if (predicate(player))
					{
						yield return player;
					}
				}
			}

			players = GetPlayers();
			return null;
		}

		private string[] PlayersFromSelector(Player sender, string selector, SearchFilter filter, Dictionary<string, string> switches, out IEnumerable<Player> players)
		{
			string[] filterError = PlayersFromFilter(sender, filter, switches, out IEnumerable<Player> selectedPlayers);
			if (filterError != null)
			{
				players = null;
				return filterError;
			}

			players = PlayersFromSelector(selector, selectedPlayers);
			return null;
		}

		public static IEnumerable<Player> PlayersFromSelector(string selector, IEnumerable<Player> playerPool)
		{
			Func<Player, bool> predicate;
			bool single;

			if (selector == "*")
			{
				predicate = player => true;
				single = false;
			}
			else if (int.TryParse(selector, out int playerId))
			{
				predicate = player => player.PlayerId == playerId;
				single = true;
			}
			else
			{
				selector = selector.ToLower();

				IEnumerable<Player> GetClosestPlayer()
				{
					int closestDiff = int.MaxValue;
					Player closestPlayer = null;

					foreach (Player player in playerPool)
					{
						if (player.Name.ToLower().Contains(selector))
						{
							int diff = player.Name.Length - selector.Length;

							if (diff < closestDiff)
							{
								if (diff == 0)
								{
									yield return player;
									yield break;
								}

								closestDiff = diff;
								closestPlayer = player;
							}
						}
					}

					if (closestPlayer != null)
					{
						yield return closestPlayer;
					}
				}

				return GetClosestPlayer();
			}

			IEnumerable<Player> GetSelectedPlayers()
			{
				foreach (Player player in playerPool)
				{
					if (predicate(player))
					{
						yield return player;
						if (single)
						{
							break;
						}
					}
				}
			}

			return GetSelectedPlayers();
		}

		private static List<string> NormalizeArguments(IEnumerable<string> args, out Dictionary<string, string> switches)
		{
			List<string> normalizedArgs = new List<string>();
			switches = new Dictionary<string, string>();

			string lastSwitch = null;
			foreach (string arg in args)
			{
				if (arg.StartsWith("--")) // Double (--)
				{
					lastSwitch = arg.Substring(2);
				}
				else if (arg.StartsWith("-")) // Single (-)
				{
					lastSwitch = arg.Substring(1);
				}
				else
				{
					if (lastSwitch != null)
					{
						switches.Add(lastSwitch, arg);

						lastSwitch = null;
					}
					else
					{
						normalizedArgs.Add(arg);
					}
				}
			}

			return normalizedArgs;
		}

		public string[] OnCall(ICommandSender sender, string[] argsArray)
		{
			Player senderPlayer = sender as Player;
			if (senderPlayer != null && !owner.whitelist.Contains(senderPlayer.GetRankName()))
			{
				return new[]
				{
					"You do not have permissions to use this holy command."
				};
			}

			List<string> args = NormalizeArguments(argsArray, out Dictionary<string, string> switches);

			if (args.Count < 1)
			{
				return new[]
				{
					"At least 1 argument is needed (earrape), defaulting to global earrape."
				};
			}

			BaseEarrape earrape = owner.Earrapes.FirstOrDefault(x => x.Info.Name == args[0]);
			if (earrape == null)
			{
				return new[]
				{
					"Earrape not found."
				};
			}

			IEnumerable<Player> players;

			SearchFilter filter = SearchFilter.Global;
			if (switches.TryGetValue("filter", out string filterData) || switches.TryGetValue("f", out filterData))
			{
				filter = _filters[filterData.ToLower()];
			}

			if (switches.TryGetValue("selector", out string selectorData) || switches.TryGetValue("s", out selectorData))
			{
				string[] selectorError = PlayersFromSelector(senderPlayer, selectorData, filter, switches, out players);
				if (selectorError != null) return selectorError;
			}
			else
			{
				players = owner.Server.GetPlayers();
			}

			Player[] playersArray = players.ToArray();

			int count = 1;
			if (switches.TryGetValue("count", out string countData) || switches.TryGetValue("c", out countData))
			{
				if (!int.TryParse(countData, out count))
				{
					return new[]
					{
						"Invalid count."
					};
				}
			}

			float offset = 0;
			if (switches.TryGetValue("offset", out string offsetData) || switches.TryGetValue("o", out offsetData))
			{
				if (!float.TryParse(offsetData, out offset))
				{
					return new[]
					{
						"Invalid offset."
					};
				}
			}

			return earrape.EarrapePlayers(playersArray, args.Skip(1).ToArray(), count, offset, switches) ?? new[]
			{
				$"Applied {count} earrape loop(s) with an offset of {offset} to {playersArray.Length} player(s)" +
				(playersArray.Length > 0 ? ":\n" + string.Join("\n", playersArray.Select(x => x.Name)) : ".")
			};
		}

		public string GetUsage() => usage;

		public string GetCommandDescription() => "Makes it impossible for loud players such as Chaos to be heard by victims of this command. Forever.";
	}
}