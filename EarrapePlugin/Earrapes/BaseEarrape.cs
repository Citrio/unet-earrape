using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MEC;
using Smod2.API;
using UnityEngine;
using UnityEngine.Networking;

namespace EarrapePlugin.Earrapes
{
	public abstract class BaseEarrape
	{
		protected uint UNetId { get; }
		protected int UNetChannel { get; }

		public UNetEarrape Owner { get; }
		public EarrapeInfo Info { get; }

		protected BaseEarrape(UNetEarrape owner, EarrapeInfo info, Type rpcSource, string rpcName)
		{
			Owner = owner;
			Info = info;

			string fullRpcConstantName = "kRpc" + rpcName;
			UNetId =
				(uint)(int)(
					rpcSource.GetField(fullRpcConstantName, BindingFlags.Static | BindingFlags.NonPublic)?.GetValue(null) ??
					throw new MissingFieldException(rpcSource.Name, fullRpcConstantName)
	           );

			UNetChannel =
				(
					(rpcSource.GetMethod(rpcName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic) ??
					throw new MissingMethodException(rpcSource.Name, rpcName))

					.GetCustomAttribute<ClientRpcAttribute>() ??
					throw new InvalidOperationException($"Method did not have a {nameof(ClientRpcAttribute)} attached in order to get the channel.")
				).channel;
		}

		public abstract string[] EarrapePlayers(IEnumerable<Player> players, string[] args, int count, float offset, Dictionary<string, string> switches);

		protected static bool TryParseBool(string b, out bool result, bool def = true)
		{
			switch (b?.ToLower())
			{
				case null:
					result = def;
					break;

				case "true":
				case "1":
				case "yes":
					result = true;
					break;

				case "false":
				case "0":
				case "no":
					result = false;
					break;

				default:
					result = default(bool);
					return false;
			}

			return true;
		}

		protected static bool TryParseInt(string i, out int result, int def = 0)
		{
			switch (i)
			{
				case null:
					result = def;
					break;

				default:
					if (!int.TryParse(i, out result)) return false;
					break;
			}

			return true;
		}

		protected void WriteAllPlayers(IEnumerable<Player> players, Action<GameObject, TargetRpcWriter> parameterWriter, int count, float offset) =>
			WriteAllPlayers(players.Select(x => (GameObject) x.GetGameObject()), parameterWriter, count, offset, (GameObject)null);

		protected void WriteAllPlayers(IEnumerable<Player> players, Action<GameObject, TargetRpcWriter> parameterWriter, int count, float offset, Player sourcePlayer) =>
			WriteAllPlayers(players.Select(x => (GameObject) x.GetGameObject()), parameterWriter, count, offset, sourcePlayer?.GetGameObject() as GameObject);

		protected void WriteAllPlayers(IEnumerable<Player> players, Action<GameObject, TargetRpcWriter> parameterWriter, int count, float offset, GameObject sourcePlayer) =>
			WriteAllPlayers(players.Select(x => (GameObject) x.GetGameObject()), parameterWriter, count, offset, sourcePlayer);

		protected void WriteAllPlayers(IEnumerable<GameObject> players, Action<GameObject, TargetRpcWriter> parameterWriter, int count, float offset) =>
			WriteAllPlayers(players, parameterWriter, count, offset, (GameObject)null);

		protected void WriteAllPlayers(IEnumerable<GameObject> players, Action<GameObject, TargetRpcWriter> parameterWriter, int count, float offset, Player sourcePlayer) =>
			WriteAllPlayers(players, parameterWriter, count, offset, sourcePlayer?.GetGameObject() as GameObject);

		protected void WriteAllPlayers(IEnumerable<GameObject> players, Action<GameObject, TargetRpcWriter> parameterWriter, int count, float offset, GameObject sourcePlayer)
		{
			NetworkInstanceId source =
				sourcePlayer == null ?
					default(NetworkInstanceId) :
					sourcePlayer.GetComponent<NetworkIdentity>().netId;

			foreach (GameObject gameObject in players)
			{
				if (sourcePlayer == null)
				{
					source = gameObject.GetComponent<NetworkIdentity>().netId;
				}

				Timing.RunCoroutine(_WritePlayerOffset(gameObject, parameterWriter, count, offset, source));
			}
		}

		private IEnumerator<float> _WritePlayerOffset(GameObject player, Action<GameObject, TargetRpcWriter> parameterWriter, int count, float offset, NetworkInstanceId source)
		{
			bool useOffset = Mathf.Round(offset * 100) >= 1; // Float imprecision :CoolApprovalBanana:

			for (int i = 0; i < count; i++)
			{
				using (TargetRpcWriter writer = new TargetRpcWriter(player, UNetId, source, UNetChannel))
				{
					parameterWriter?.Invoke(player, writer);
				}

				if (useOffset)
				{
					yield return Timing.WaitForSeconds(offset);
				}
			}
		}
	}
}