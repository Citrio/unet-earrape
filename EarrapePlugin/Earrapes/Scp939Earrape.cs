using System.Collections.Generic;
using System.Linq;
using Smod2.API;

namespace EarrapePlugin.Earrapes
{
	[EarrapeInfo("939")]
	public class Scp939Earrape : SourceEarrape
	{
		public Scp939Earrape(UNetEarrape owner, EarrapeInfo info) : base(owner, info, typeof(Scp939PlayerScript), "RpcShoot") { }

		public override string[] EarrapePlayersWithSource(Player[] players, IEnumerable<Player> sources, string[] args, int count, float offset, Dictionary<string, string> switches)
		{
			WriteAllPlayers(
				players,
				null,
				count,
				offset,
				sources.Where(x => x.TeamRole.Role == Role.SCP_939_53 || x.TeamRole.Role == Role.SCP_939_89)
			);

			return null;
		}
	}
}