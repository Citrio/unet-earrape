using System.Collections.Generic;
using System.Linq;
using Smod2.API;
using Object = UnityEngine.Object;

namespace EarrapePlugin.Earrapes
{
	[EarrapeInfo("elevator")]
	public class ElevatorEarrape : BaseEarrape
	{
		public ElevatorEarrape(UNetEarrape owner, EarrapeInfo info) : base(owner, info, typeof(Lift), "RpcPlayMusic") { }

		public override string[] EarrapePlayers(IEnumerable<Player> players, string[] args, int count, float offset, Dictionary<string, string> switches)
		{
			Player[] playersArray = players.ToArray();
			foreach (Lift lift in Object.FindObjectsOfType<Lift>())
			{
				WriteAllPlayers(
					playersArray,
					null,
					count,
					offset,
					lift.gameObject
				);
			}

			return null;
		}
	}
}