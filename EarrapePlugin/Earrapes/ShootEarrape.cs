using System.Collections.Generic;
using System.Linq;
using Smod2.API;
using UnityEngine;

namespace EarrapePlugin.Earrapes
{
	[EarrapeInfo("shoot")]
	public class ShootEarrape : SourceEarrape
	{
		public ShootEarrape(UNetEarrape owner, EarrapeInfo info) : base(owner, info, typeof(WeaponManager), "RpcConfirmShot") { }

		public override string[] EarrapePlayersWithSource(Player[] players, IEnumerable<Player> sources, string[] args, int count, float offset, Dictionary<string, string> switches)
		{
			int? weaponId = null;
			if (switches.TryGetValue("weapon", out string weapon))
			{
				if (!TryParseInt(weapon, out int weaponIdNonNull)) return new[] {"Invalid weapon ID."};
				weaponId = weaponIdNonNull;
			}

			Dictionary<GameObject, int> weaponPairs =
				sources
					.Select(x =>
					{
						GameObject gameObject = (GameObject) x.GetGameObject();
						return new KeyValuePair<GameObject, int>(gameObject, gameObject.GetComponent<WeaponManager>().curWeapon);
					})
					.Where(x => weaponId != null ? x.Value == weaponId : x.Value != -1)
					.ToDictionary(x => x.Key, x => x.Value);

			foreach (KeyValuePair<GameObject, int> weaponSource in weaponPairs)
			{
				WriteAllPlayers(
					players,
					(player, writer) =>
					{
						writer.Write(false); // Hitmarker (not needed)
						writer.WritePackedUInt32((uint) weaponSource.Value); // Weapon to fire
					},
					count,
					offset,
					weaponSource.Key
				);
			}

			return null;
		}
	}
}