using System.Collections.Generic;
using System.Linq;
using Smod2.API;
using UnityEngine;

namespace EarrapePlugin.Earrapes
{
	[EarrapeInfo("fall")]
	public class FallDamageEarrape : SourceEarrape
	{
		public FallDamageEarrape(UNetEarrape owner, EarrapeInfo info) : base(owner, info, typeof(FallDamage), "RpcDoSound") { }

		public override string[] EarrapePlayersWithSource(Player[] players, IEnumerable<Player> sources, string[] args, int count, float offset, Dictionary<string, string> switches)
		{
			WriteAllPlayers(
				players,
				(player, writer) =>
				{
					// The ClientRpc does nothing with these
					writer.Write(Vector3.zero);
					writer.Write(0f);
				},
				count,
				offset,
				sources
			);

			return null;
		}
	}
}