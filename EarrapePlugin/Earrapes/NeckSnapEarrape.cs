using System.Collections.Generic;
using Smod2.API;

namespace EarrapePlugin.Earrapes
{
	[EarrapeInfo("neck")]
	public class NeckSnapEarrape : SourceEarrape
	{
		public NeckSnapEarrape(UNetEarrape owner, EarrapeInfo info) : base(owner, info, typeof(Scp173PlayerScript), "RpcSyncAudio") { }

		public override string[] EarrapePlayersWithSource(Player[] players, IEnumerable<Player> sources, string[] args, int count, float offset, Dictionary<string, string> switches)
		{
			WriteAllPlayers(
				players,
				null,
				count,
				offset,
				sources
			);

			return null;
		}
	}
}