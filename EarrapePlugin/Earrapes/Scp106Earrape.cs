using System.Collections.Generic;
using Smod2.API;

namespace EarrapePlugin.Earrapes
{
	[EarrapeInfo("106")]
	public class Scp106Earrape : BaseEarrape
	{
		public Scp106Earrape(UNetEarrape owner, EarrapeInfo info) : base(owner, info, typeof(PlayerInteract), "RpcContain106") { }

		public override string[] EarrapePlayers(IEnumerable<Player> players, string[] args, int count, float offset, Dictionary<string, string> switches)
		{
			WriteAllPlayers(
				players,
				(player, writer) => writer.Write(PlayerManager.localPlayer), // Write the target of the recontainment. Setting the target not as the player it's calling the RPC of makes it only run screams and not a foreach	wesmart
				count,
				offset,
				PlayerManager.localPlayer
			);

			return null;
		}
	}
}