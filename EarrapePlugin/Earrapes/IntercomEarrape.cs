using System.Collections.Generic;
using RemoteAdmin;
using Smod2.API;

namespace EarrapePlugin.Earrapes
{
	[EarrapeInfo("intercom")]
	public class IntercomEarrape : BaseEarrape
	{
		public IntercomEarrape(UNetEarrape owner, EarrapeInfo info) : base(owner, info, typeof(Intercom), "RpcPlaySound") { }

		public override string[] EarrapePlayers(IEnumerable<Player> players, string[] args, int count, float offset, Dictionary<string, string> switches)
		{
			if (!TryParseBool(switches.TryGetDefault("end"), out bool end)) return new[] {"Invalid end flag."};

			WriteAllPlayers(
				players,
				(player, writer) =>
				{
					writer.Write(!end); // Whether or not to play the start of the announcement
					writer.Write(QueryProcessor.Localplayer.PlayerId); // Transmitter, doesnt show long enough to warrant name change.
				},
				count,
				offset,
				Intercom.host.gameObject
			);

			return null;
		}
	}
}