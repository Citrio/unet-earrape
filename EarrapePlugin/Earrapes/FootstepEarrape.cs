using System.Collections.Generic;
using System.Linq;
using Smod2.API;
using UnityEngine;

namespace EarrapePlugin.Earrapes
{
	[EarrapeInfo("foot")]
	public class FootstepEarrape : SourceEarrape
	{
		public FootstepEarrape(UNetEarrape owner, EarrapeInfo info) : base(owner, info, typeof(FootstepSync), "RpcSyncFoot") { }

		public override string[] EarrapePlayersWithSource(Player[] players, IEnumerable<Player> sources, string[] args, int count, float offset, Dictionary<string, string> switches)
		{
			if (!TryParseBool(switches.TryGetDefault("running"), out bool running)) return new[] {"Invalid running flag."};

			WriteAllPlayers(
				players,
				(player, writer) => writer.Write(running),
				count,
				offset,
				sources
			);

			return null;
		}
	}
}