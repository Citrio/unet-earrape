using System.Collections.Generic;
using Smod2.API;

namespace EarrapePlugin.Earrapes
{
	[EarrapeInfo("chaos")]
	public class ChaosDrumsEarrape : BaseEarrape
	{
		public ChaosDrumsEarrape(UNetEarrape owner, EarrapeInfo info) : base(owner, info, typeof(MTFRespawn), "RpcAnnouncCI") { }

		public override string[] EarrapePlayers(IEnumerable<Player> players, string[] args, int count, float offset, Dictionary<string, string> switches)
		{
			WriteAllPlayers(
				players,
				null,
				count,
				offset,
				PlayerManager.localPlayer
			);

			return null;
		}
	}
}