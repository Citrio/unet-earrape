using System.Collections.Generic;
using System.Linq;
using Smod2.API;
using UnityEngine;

namespace EarrapePlugin.Earrapes
{
	[EarrapeInfo("micro")]
	public class MicroHIDEarrape : SourceEarrape
	{
		public MicroHIDEarrape(UNetEarrape owner, EarrapeInfo info) : base(owner, info, typeof(MicroHID_GFX), "RpcSyncAnim") { }

		public override string[] EarrapePlayersWithSource(Player[] players, IEnumerable<Player> sources, string[] args, int count, float offset, Dictionary<string, string> switches)
		{
			WriteAllPlayers(
				players,
				null,
				count,
				offset,
				sources
			);

			return null;
		}
	}
}