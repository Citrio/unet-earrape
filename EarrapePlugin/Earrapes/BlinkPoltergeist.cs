using System.Collections.Generic;
using System.Linq;
using Smod2.API;

namespace EarrapePlugin.Earrapes
{
	[EarrapeInfo("blink")]
	public class BlinkPoltergeist : BaseEarrape
	{
		public BlinkPoltergeist(UNetEarrape owner, EarrapeInfo info) : base(owner, info, typeof(Scp173PlayerScript), "RpcBlinkTime") { }

		public override string[] EarrapePlayers(IEnumerable<Player> players, string[] args, int count, float offset, Dictionary<string, string> switches)
		{
			WriteAllPlayers(
				players.Where(x => x.TeamRole.Role != Role.SCP_173),
				null,
				count,
				offset
			);

			return null;
		}
	}
}