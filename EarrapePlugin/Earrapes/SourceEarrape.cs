using System;
using System.Collections.Generic;
using System.Linq;
using Smod2.API;
using UnityEngine;

namespace EarrapePlugin.Earrapes
{
	public abstract class SourceEarrape : BaseEarrape
	{
		protected SourceEarrape(UNetEarrape owner, EarrapeInfo info, Type rpcSource, string rpcName) : base(owner, info, rpcSource, rpcName) { }

		public override string[] EarrapePlayers(IEnumerable<Player> players, string[] args, int count, float offset, Dictionary<string, string> switches)
		{
			Player[] playersArray = players.ToArray();
			IEnumerable<Player> sources =
				switches.TryGetValue("source", out string source) ?
					RootCommand.PlayersFromSelector(source, Owner.Server.GetPlayers()) :
					playersArray;

			return EarrapePlayersWithSource(playersArray, sources, args, count, offset, switches);
		}

		public abstract string[] EarrapePlayersWithSource(Player[] players, IEnumerable<Player> sources, string[] args, int count, float offset, Dictionary<string, string> switches);

		protected void WriteAllPlayers(IEnumerable<Player> players, Action<GameObject, TargetRpcWriter> parameterWriter, int count, float offset, IEnumerable<Player> sources) =>
			WriteAllPlayers(players.Select(x => (GameObject) x.GetGameObject()).ToArray(), parameterWriter, count, offset, sources.Select(x => (GameObject) x.GetGameObject()));

		protected void WriteAllPlayers(IEnumerable<Player> players, Action<GameObject, TargetRpcWriter> parameterWriter, int count, float offset, IEnumerable<GameObject> sources) =>
			WriteAllPlayers(players.Select(x => (GameObject) x.GetGameObject()).ToArray(), parameterWriter, count, offset, sources);

		protected void WriteAllPlayers(GameObject[] players, Action<GameObject, TargetRpcWriter> parameterWriter, int count, float offset, IEnumerable<Player> sources) =>
			WriteAllPlayers(players, parameterWriter, count, offset, sources.Select(x => (GameObject) x.GetGameObject()));

		protected void WriteAllPlayers(GameObject[] players, Action<GameObject, TargetRpcWriter> parameterWriter, int count, float offset, IEnumerable<GameObject> sources)
		{
			foreach (GameObject source in sources)
			{
				WriteAllPlayers(
					players,
					parameterWriter,
					count,
					offset,
					source
				);
			}
		}
	}
}