using System.Collections.Generic;
using Smod2.API;

namespace EarrapePlugin.Earrapes
{
	[EarrapeInfo("lcz")]
	public class LczDeconEarrape : BaseEarrape
	{
		public LczDeconEarrape(UNetEarrape owner, EarrapeInfo info) : base(owner, info, typeof(DecontaminationLCZ), "RpcPlayAnnouncement") { }

		public override string[] EarrapePlayers(IEnumerable<Player> players, string[] args, int count, float offset, Dictionary<string, string> switches)
		{
			if (!TryParseInt(switches.TryGetDefault("id"), out int announcement, 4) || announcement < 0 || 5 < announcement) return new[] {"Invalid announcement ID."};

			if (!TryParseBool(switches.TryGetDefault("global"), out bool global)) return new[] {"Invalid global flag."};

			WriteAllPlayers(
				players,
				(player, writer) =>
				{
					writer.WritePackedUInt32((uint) announcement); // ID of announcement to play
					writer.Write(global); // Broadcast to those outside of LCZ
				},
				count,
				offset,
				PlayerManager.localPlayer
			);

			return null;
		}
	}
}