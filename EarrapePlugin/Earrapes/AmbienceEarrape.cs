using System.Collections.Generic;
using Smod2.API;

namespace EarrapePlugin.Earrapes
{
	[EarrapeInfo("ambient")]
	public class AmbienceEarrape : BaseEarrape
	{
		public AmbienceEarrape(UNetEarrape owner, EarrapeInfo info) : base(owner, info, typeof(AmbientSoundPlayer), "RpcPlaySound") { }

		public override string[] EarrapePlayers(IEnumerable<Player> players, string[] args, int count, float offset, Dictionary<string, string> switches)
		{
			if (!TryParseInt(switches.TryGetDefault("id"), out int ambientSound, 24) || ambientSound < 0 || 24 < ambientSound) return new[] {"Invalid ambient sound ID."};

			WriteAllPlayers(
				players,
				(player, writer) => writer.Write(ambientSound),
				count,
				offset,
				PlayerManager.localPlayer
			);

			return null;
		}
	}
}