using System;

namespace EarrapePlugin
{
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	public class EarrapeInfo : Attribute
	{
		public string Name { get; }

		public EarrapeInfo(string name)
		{
			Name = name;
		}
	}
}