using System.Collections.Generic;

namespace EarrapePlugin
{
	internal static class Extensions
	{
		public static TValue TryGetDefault<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key) =>
			dictionary.TryGetValue(key, out TValue value) ? value : default(TValue);
	}
}